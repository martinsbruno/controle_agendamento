/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.bean;

import br.com.softline.padrao.converter.Contexto;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author bruno-martins
 */
public class PadraoBean {

    @PersistenceContext
    protected EntityManager em;

    class AgrupamentoObjetos {

        public Class clazz;
        public List lista = new ArrayList();

        @Override
        public int hashCode() {
            int hash = 7;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final AgrupamentoObjetos other = (AgrupamentoObjetos) obj;
            if (!Objects.equals(this.clazz, other.clazz)) {
                return false;
            }
            return true;
        }

    }

    public Object findByPrimaryKey(Class classe, Object obj) {
        Object o = em.find(classe, obj);
        return o;
    }

    public Object findByPrimaryKey(Class classe, Object obj, String condicao) {
        Field[] field = classe.getDeclaredFields();
        Field fieldChavePrimaria = null;

        for (Field f : field) {
            if (f.isAnnotationPresent(Id.class)) {
                fieldChavePrimaria = f;
                break;
            }
        }

        String consulta = "select o from " + classe.getSimpleName() + " o where o."
                + fieldChavePrimaria.getName() + " = :valor " + condicao;
        try {
            Object o = em.createQuery(consulta).setParameter("valor", obj).getSingleResult();
            return o;
        } catch (Exception e) {
            return null;
        }
    }

    public Object findByDescricao(Class classe, String valorDesc, String campoDesc, String condicao) {
        try {

            String consulta = "select o from " + classe.getSimpleName() + " o where o." + campoDesc + " like '%" + valorDesc + "%'  "
                    + " " + condicao + "";

            List result = em.createQuery(consulta).getResultList();

            if (result.isEmpty()) {
                return null;
            }

            if (result.size() > 1) {
                return null;
            } else {
                return result.get(0);
            }

        } catch (Exception e) {
            return null;
        }
    }

    public void create(Object obj) {
        try {
            em.persist(obj);
            em.flush();

            Field field = getFieldId(obj.getClass());
            field.setAccessible(true);

        } catch (Exception e) {
            addErro(e.toString());
        }
    }

    public void update(Object obj) {

        try {
            em.clear();
            em.merge(obj);
            em.flush();
        } catch (Exception e) {
            addErro(e.toString());
        }

    }

    public void delete(Object obj) {

        int result = 0;
        try {
            obj = em.merge(obj);
            em.remove(obj);
            em.flush();
        } catch (Exception e) {
            addErro(e.toString());
        }

    }

    public void refreshModel(Object o) {
        try {
            em.refresh(o);
        } catch (Exception e) {
        }
    }

    public List findByQueries(String consulta) {
        return findByQueries(consulta, 0, 0);
    }

    private Field getFieldId(Class clazz) {

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Id.class)) {
                return field;
            }
        }
        return null;
    }

    public void addErro(String mensagem) {
        try {
            em.getTransaction().rollback();
        } catch (Exception e) {
        }

    }

    public List findByQueries(String consulta, int quantRegistros, int posicao) {
        if (quantRegistros == 0 && posicao == 0) {
            return em.createQuery(consulta).
                    getResultList();
        } else {
            return em.createQuery(consulta).
                    setMaxResults(quantRegistros).
                    setFirstResult(posicao).
                    getResultList();
        }
    }

    public Long getCountResult(String consulta) {
        int iFrom = consulta.toUpperCase().indexOf("FROM");
        String sResult = "select DISTINCT COUNT(o) " + consulta.substring(iFrom);
        Long quant;
        try {
            quant = (Long) em.createQuery(sResult).getSingleResult();
        } catch (NoResultException ex) {
            quant = 0L;
        }
        return quant;
    }

    private boolean isCascadeAll(OneToMany oneToMany) {

        CascadeType[] cascadeTypes = oneToMany.cascade();
        boolean isCascadeAll = false;
        for (CascadeType cascadeType : cascadeTypes) {
            if (cascadeType.equals(CascadeType.ALL)) {
                isCascadeAll = true;
                break;
            }
        }

        return isCascadeAll;
    }

    private boolean isCascadeAll(ManyToOne manyToOne) {

        CascadeType[] cascadeTypes = manyToOne.cascade();
        boolean isCascadeAll = false;
        for (CascadeType cascadeType : cascadeTypes) {
            if (cascadeType.equals(CascadeType.ALL)) {
                isCascadeAll = true;
                break;
            }
        }

        return isCascadeAll;
    }

    public String getIp() {
        HttpServletRequest rq = (HttpServletRequest) Contexto.getExternalContext().getRequest();

        String ipAddress = rq.getRemoteAddr();

        if (ipAddress == null) {
            ipAddress = "127.0.0.1";
        }

        if (ipAddress.contains("0:0:0:")) {
            ipAddress = "127.0.0.1";
        }

        return ipAddress;
    }

    public String getIpFisico() {
        String ipAddress = null;
        Enumeration<NetworkInterface> net = null;
        try {
            net = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        while (net.hasMoreElements()) {
            NetworkInterface element = net.nextElement();
            Enumeration<InetAddress> addresses = element.getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress ip = addresses.nextElement();

                if (ip.isSiteLocalAddress()) {
                    ipAddress = ip.getHostAddress();
                }
            }
        }
        return ipAddress;
    }

    public String getIpRelativo() {
        String ip = "";
        InetAddress addr;
        try {
            addr = InetAddress.getLocalHost();
            ip = addr.getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PadraoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ip;
    }

    public String getnmMaquinaIpReal() {
        String nmMaquinaIpReal = "";
        try {
            InetAddress myself = InetAddress.getLocalHost();
            nmMaquinaIpReal = myself.getHostName();
            //ipReal = myself.getHostAddress();
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        return nmMaquinaIpReal;
    }
}
