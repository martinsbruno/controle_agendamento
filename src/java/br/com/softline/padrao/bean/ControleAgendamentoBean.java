/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.bean;

import br.com.softline.padrao.model.Cliente;
import br.com.softline.padrao.model.ControleAgendamento;
import br.com.softline.padrao.model.ErrosSistema;
import br.com.softline.padrao.model.Log;
import br.com.softline.padrao.model.Suporte;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author bruno-martins
 */
@Stateless
@LocalBean
public class ControleAgendamentoBean extends PadraoBean implements IPadraoBean {

    @Override
    public Object instanceObject() {
        return new ControleAgendamento();
    }

    public boolean validarSuporteIp(String ip) {
        List<Suporte> result = em.createQuery("SELECT o FROM Suporte o WHERE o.ipMaquina='" + ip + "'").getResultList();
        boolean resp = false;
        if (!result.isEmpty()) {
            resp = true;
        }
        return resp;
    }

    public List<ControleAgendamento> findAllTask() {
        return em.createQuery("SELECT o FROM ControleAgendamento o ORDER BY o.statusAgendamento,o.dataAgendamento DESC").getResultList();
    }

    public List<ErrosSistema> findAllError() {
        return em.createQuery("SELECT o FROM ErrosSistema o ORDER BY o.status,o.dataRegistro DESC").getResultList();
    }
    
    public List<Log> findAllLogs() {
        return em.createQuery("SELECT o FROM Log o ORDER BY o.dataHoraLog DESC").getResultList();
    }

    public List<Cliente> getCompleteCliente(String texto) {
        return em.createQuery("SELECT o FROM Cliente o WHERE o.nmCliente like '%" + texto + "%' or o.nmEmpresaCliente like '%" + texto + "%'").getResultList();
    }

    public List<Suporte> getCompleteSuporte(String texto) {
        return em.createQuery("SELECT o FROM Suporte o WHERE o.nmSuporte like '%" + texto + "%' and o.ativo ='1'").getResultList();
    }

}
