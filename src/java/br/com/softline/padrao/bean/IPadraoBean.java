/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.bean;

import java.util.List;

/**
 *
 * @author bruno-martins
 */
public interface IPadraoBean {

    public void create(Object obj);

    public void update(Object obj);

    public void delete(Object obj);

    public List findByQueries(String consulta, int quantRegistros, int posicao);

    public Object findByPrimaryKey(Class classe, Object obj, String condicao);

    public Object findByPrimaryKey(Class classe, Object obj);

    public Object instanceObject();

    public Long getCountResult(String consulta);

    public Object findByDescricao(Class classe, String valorDesc, String campoDesc, String condicao);

}
