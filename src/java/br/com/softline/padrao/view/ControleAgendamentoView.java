/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.view;

import br.com.softline.padrao.bean.ControleAgendamentoBean;
import br.com.softline.padrao.model.Cliente;
import br.com.softline.padrao.model.ControleAgendamento;
import br.com.softline.padrao.model.ErrosSistema;
import br.com.softline.padrao.model.Log;
import br.com.softline.padrao.model.Modulos;
import br.com.softline.padrao.model.Pastas;
import br.com.softline.padrao.model.Servidor;
import br.com.softline.padrao.model.Suporte;
import br.com.softline.padrao.model.Telefone;
import br.com.sotline.padrao.temp.Historico;
import br.com.sotline.padrao.temp.Navegacao;
import br.com.sotline.padrao.temp.UploadTemp;
import br.com.sotline.padrao.temp.Utils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author bruno-martins
 */
@ViewScoped
@ManagedBean
public class ControleAgendamentoView {

    @EJB
    private ControleAgendamentoBean controleAgendamentoBean;
    private Cliente cliente;
    private Suporte suporte;
    private Modulos modulos;
    private Telefone telefone;
    private Servidor servidor;
    private Pastas pastas;
    private ControleAgendamento controleAgendamento;
    private ErrosSistema errosSistema;
    private Integer tabProcesso;
    private boolean renderCliente;
    private boolean renderTelefoneCliente;
    private boolean renderSuporte;
    private boolean renderModulos;
    private boolean renderServidor;
    private boolean renderPastas;
    private boolean renderTarefas;
    private boolean renderErrosSistema;
    private boolean renderLogsSistema;
    private boolean disableIpSuporte;
    private boolean disableinputEditarSuporte;
    private boolean disableButtonBack;
    private boolean disableButtonNext;
    private boolean disableInputServer;
    private boolean respostaUpload;
    private List<Cliente> listCliente;
    private List<Log> listaLog;
    private List<Suporte> listSuporte;
    private List<Modulos> listModulos;
    private List<Pastas> listPastas;
    private List<ControleAgendamento> listTarefas;
    private List<Telefone> listTelefonesRemovido;
    private List<ErrosSistema> listErrosSistema;
    private Integer tabCadastro;
    private String dataAtual;
    private Integer statusValidacao;
    private Navegacao navegacao;
    private String barraConfig;

    public String getTitulo() {
        dataAtual = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR")).format(new Date());
        iniciar();
        return "Controle Agendamento Tarefas";
    }

    public void iniciar() {
        String so = System.getProperty("os.name").toLowerCase();
        barraConfig = so.equals("linux") ? "/" : "'\'";
        tarefa();
    }

    //****************************** MENU CADASTROS ****************************
    public void cadastro() {
        tabProcesso = 1;
        clienteTab();
    }

    //************* cliente ***************
    public void carregarCliente() {
        listCliente = controleAgendamentoBean.findByQueries("Select o from Cliente o");
        for (Cliente c : listCliente) {
            c.setTelefoneCliente(c.getListTelefoneCliente().isEmpty() ? "Não Informado" : c.getListTelefoneCliente().get(0).getNumTelefone());
        }
    }

    public List<Cliente> completeCliente(String texto) {
        List<Cliente> result = new ArrayList<>();
        texto = texto.trim();
        if (texto != null && !texto.equals("") && texto.length() >= 3) {
            result = controleAgendamentoBean.getCompleteCliente(texto);
        }
        return result;
    }

    public void clienteTab() {
        tabCadastro = 1;
        renderCliente = true;
        cliente = null;
        telefone = null;
        carregarCliente();

    }

    public void adicionarCliente() {
        renderCliente = false;
        renderTelefoneCliente = true;
        telefone = null;
        cliente = new Cliente();
    }

    public void editarCliente() {
        listTelefonesRemovido = new ArrayList<>();
        renderTelefoneCliente = true;
        renderCliente = false;
    }

    public void deletarCliente() {
        controleAgendamentoBean.delete(cliente);
        carregarCliente();
        addSuccessMessage("Cliente Removido com Sucesso !!");
    }

    public void confirmarCliente() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (cliente.getNmCliente() == null || cliente.getNmCliente().equals("")) {
            addErrorMessages("Nome do Cliente nao Informado !!");
            return;
        }
        cliente.setNmEmpresaCliente(cliente.getNmEmpresaCliente() != null && !cliente.getNmEmpresaCliente().equals("") ? cliente.getNmEmpresaCliente() : "Não Informado");
        if (cliente.getId() == null) {
            controleAgendamentoBean.create(cliente);
            addSuccessMessage("Cliente Adicionado com sucesso !!");
        } else {
            controleAgendamentoBean.update(cliente);
            if (!listTelefonesRemovido.isEmpty()) {
                for (Telefone t : listTelefonesRemovido) {
                    controleAgendamentoBean.delete(t);
                }
            }
            listTelefonesRemovido = new ArrayList<>();
            addSuccessMessage("Cliente Atualizado com sucesso !!");
        }
        context.addCallbackParam("sucesso", true);
        cancelarCliente();

    }

    public void cancelarCliente() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (!getListTelefonesRemovido().isEmpty()) {
            addErrorMessages("Confirme o Registro para a atualização do Cliente !!");
            return;
        }
        cliente = null;
        telefone = null;
        renderCliente = true;
        context.addCallbackParam("sucesso", true);
        carregarCliente();
    }

    public void adicionarTelefoneCliente() {
        telefone = new Telefone();
        telefone.setTipoTelefone("1");
        renderTelefoneCliente = false;
    }

    public void editarTelefoneCliente() {
        renderTelefoneCliente = false;
    }

    public void deletarTelefoneCliente() {
        cliente.getListTelefoneCliente().remove(telefone);
        if (telefone.getId() != null) {
            listTelefonesRemovido.add(telefone);
        }
    }

    public void confirmarTelefoneCliente() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (telefone.getTipoTelefone() == null || telefone.getTipoTelefone().equals("")) {
            addErrorMessages("Tipo de Telefone não Informado!!");
            return;
        }
        if ((telefone.getNmOperadora() == null || telefone.getNmOperadora().equals("")) && telefone.getTipoTelefone().equals("1")) {
            addErrorMessages("Operadora não Informado !!");
            return;
        }

        if (telefone.getNumTelefone() == null || telefone.getNumTelefone().equals("")) {
            addErrorMessages("Telefone não Informado !!");
            return;
        }
        String numeroTelefone = telefone.getNumTelefone();
        numeroTelefone = numeroTelefone.replace("(", "");
        numeroTelefone = numeroTelefone.replace(")", "");
        numeroTelefone = numeroTelefone.replace("-", "");
        numeroTelefone = numeroTelefone.trim();
        if (numeroTelefone.length() < (telefone.getTipoTelefone().equals("1") ? 12 : 8)) {
            addErrorMessages("Informe um Número Válido !!");
            return;
        }
        if (telefone.getId() == null) {
            cliente.getListTelefoneCliente().add(telefone);
        } else {
            for (Telefone tel : cliente.getListTelefoneCliente()) {
                if (tel.getId().equals(telefone.getId())) {
                    tel = telefone;
                    break;
                }
            }
        }
        context.addCallbackParam("sucesso", true);
        cancelarTelefoneCliente();
    }

    public void cancelarTelefoneCliente() {
        telefone = null;
        renderTelefoneCliente = true;
    }

    public void tipoTelfoneOnChange() {
        telefone.setNmOperadora("");
        telefone.setNumTelefone(null);
    }

    //************* suporte ***************
    public void carregarSuporte() {
        listSuporte = controleAgendamentoBean.findByQueries("Select o from Suporte o");
        if (!controleAgendamentoBean.getIp().equals("127.0.0.1")) {
            disableIpSuporte = controleAgendamentoBean.validarSuporteIp(controleAgendamentoBean.getIp());
        } else {
            disableIpSuporte = false;
        }
    }

    public List<Suporte> completeSuporte(String texto) {
        List<Suporte> result = new ArrayList<>();
        texto = texto.trim();
        if (texto != null && !texto.equals("") && texto.length() >= 3) {
            result = controleAgendamentoBean.getCompleteSuporte(texto);
        }
        return result;
    }

    public void suporteTab() {
        tabCadastro = 2;
        renderSuporte = true;
        suporte = null;
        carregarSuporte();
    }

    public void adicionarSuporte() {
        suporte = new Suporte();
        suporte.setIpMaquina(controleAgendamentoBean.getIp());
        disableIpSuporte = true;
        renderSuporte = false;
    }

    public void editarSuporte() {
        boolean resp = suporte.getIpMaquina().equals(controleAgendamentoBean.getIp());
        if (!resp) {
            resp = controleAgendamentoBean.getIp().equals("127.0.0.1");
        }
        disableIpSuporte = resp;
        renderSuporte = false;
    }

    public void deletarSuporte() {
        disableIpSuporte = suporte.getIpMaquina().equals(controleAgendamentoBean.getIp());
        if (disableIpSuporte || controleAgendamentoBean.getIp().equals("127.0.0.1")) {
            controleAgendamentoBean.delete(suporte);
            addSuccessMessage("Suporte Removido com Sucesso !!");
        } else {
            addErrorMessages("Operação Impossibilitada !!");
        }
        carregarSuporte();
    }

    public void confirmarSuporte() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (suporte.getNmSuporte() == null || suporte.getNmSuporte().equals("")) {
            addErrorMessages("Nome do Suporte nao Informado !!");
            return;
        }
        if (suporte.getId() == null) {
            controleAgendamentoBean.create(suporte);
            addSuccessMessage("Suporte Adicionado com sucesso !!");
        } else {
            controleAgendamentoBean.update(suporte);
            addSuccessMessage("Suporte Atualizado com sucesso !!");
        }
        context.addCallbackParam("sucesso", true);
        cancelarSuporte();

    }

    public void cancelarSuporte() {
        suporte = null;
        renderSuporte = true;
        carregarSuporte();
    }

    //************* Modulos ***************
    public void carregarModulos() {
        listModulos = controleAgendamentoBean.findByQueries("Select o From Modulos o");
    }

    public List<SelectItem> getSelectModulos() {
        List<SelectItem> result = new ArrayList<>();
        List<Modulos> mod = controleAgendamentoBean.findByQueries("Select o From Modulos o");
        for (Modulos m : mod) {
            SelectItem item = new SelectItem();
            item.setLabel(m.getNmModulo());
            item.setValue(m);
            result.add(item);
        }
        return result;
    }

    public void modulosTab() {
        tabCadastro = 3;
        renderModulos = true;
        modulos = null;
        carregarModulos();
    }

    public void adicionarModulos() {
        modulos = new Modulos();
        renderModulos = false;
    }

    public void editarModulos() {
        renderModulos = false;
    }

    public void deletarModulos() {
        controleAgendamentoBean.delete(modulos);
        addSuccessMessage("Modulo Removido com Sucesso !!");
        carregarModulos();
    }

    public void confirmarModulos() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (modulos.getNmModulo() == null || modulos.getNmModulo().equals("")) {
            addErrorMessages("Nome do Módulo não Informado !!");
            return;
        }
        modulos.setNmModulo(modulos.getNmModulo().toUpperCase());
        if (modulos.getId() == null) {
            controleAgendamentoBean.create(modulos);
            addSuccessMessage("Modulo Adicionado com Sucesso !!");
        } else {
            controleAgendamentoBean.update(modulos);
            addSuccessMessage("Modulo Atualizado com Sucesso !!");
        }
        context.addCallbackParam("sucesso", true);
        cancelarModulos();
    }

    public void cancelarModulos() {
        modulos = null;
        renderModulos = true;
        carregarModulos();
    }

    //************* servidor ***************
    public void servidorTab() {
        tabCadastro = 4;
        statusValidacao = 1;
        renderPastas = true;
        disableInputServer = true;
        navegacao = null;
        carregarServidor();
    }

    public void carregarServidor() {
        List<Servidor> result = controleAgendamentoBean.findByQueries("select o from Servidor o");
        servidor = result.isEmpty() ? null : result.get(0);
        if (servidor == null) {
            servidor = new Servidor();
            disableInputServer = false;
        } else {
            validarDiretorioServidor();
        }
    }

    public void salvarDiretorioServidor() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (servidor.getDiretorio() == null || servidor.getDiretorio().equals("")) {
            addErrorMessages("Diretório Servidor não Informado !!");
            return;
        }
        if (servidor.getId() == null) {
            controleAgendamentoBean.create(servidor);
            addSuccessMessage("Diretório Servidor Salvo com Sucesso !!");
        } else {
            controleAgendamentoBean.update(servidor);
            addSuccessMessage("Diretório Servidor Atualizado com Sucesso !!");
        }
        disableInputServer = true;
        servidor.setListHistorico(null);
        context.addCallbackParam("sucesso", true);

    }

    public void editarDiretorioServidor() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        disableInputServer = false;
        context.addCallbackParam("sucesso", true);
    }

    public void validarDiretorioServidor() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        servidor.setListNavegacaoFull(new ArrayList<Navegacao>());
        servidor.setListNavegacaoVisualizada(new ArrayList<Navegacao>());

        if (servidor.getDiretorio() == null || servidor.getDiretorio().equals("")) {
            addErrorMessages("Informe um diretório !!");
            return;
        }
        File dir = new File(servidor.getDiretorio());
        if (!dir.exists()) {
            statusValidacao = 3;
            addErrorMessages("Informe um diretório Válido !!");
            return;
        }

        File[] lista = dir.listFiles();
        int i = 1;
        for (File file : lista) {
            Navegacao n = new Navegacao();
            n.setId(i);
            n.setArquivo(file);
            servidor.getListNavegacaoFull().add(n);
            i++;
        }

        if (servidor.getListNavegacaoFull().size() > 12) {
            servidor.setListNavegacaoVisualizada(servidor.getListNavegacaoFull().subList(0, 12));
            disableButtonBack = true;
            disableButtonNext = false;
        } else {
            disableButtonBack = true;
            disableButtonNext = true;
            servidor.setListNavegacaoVisualizada(servidor.getListNavegacaoFull());
        }

        statusValidacao = 2;
        pastas = null;
        renderPastas = true;
        addSuccessMessage("Diretório Válido !!");

        context.addCallbackParam("sucesso", true);
    }

    public void voltarFilePath() {
        if (!servidor.getListHistorico().isEmpty()) {
            Collections.sort(servidor.getListHistorico(), new Comparator<Historico>() {
                @Override
                public int compare(Historico o1, Historico o2) {
                    return o1.getId().compareTo(o2.getId());
                }
            });
            Collections.reverse(servidor.getListHistorico());
            Historico itemRemover = servidor.getListHistorico().get(0);
            servidor.setDiretorio(servidor.getDiretorio().replace(itemRemover.getNmHistorico(), ""));
            servidor.getListHistorico().remove(itemRemover);
            validarDiretorioServidor();
        }
    }

    public void voltarListFiles() {
        Navegacao nav = servidor.getListNavegacaoVisualizada().get(0);
        Integer index = nav.getId() - 1;
        servidor.setListNavegacaoVisualizada(null);
        servidor.setListNavegacaoVisualizada(servidor.getListNavegacaoFull().subList(index - 12, index));
        if (servidor.getListNavegacaoVisualizada().get(0).getId() == 1) {
            disableButtonBack = true;
            disableButtonNext = false;
        } else {
            disableButtonBack = false;
            disableButtonNext = false;
        }
    }

    public void avancarListFiles() {
        Navegacao nav = servidor.getListNavegacaoVisualizada().get(servidor.getListNavegacaoVisualizada().size() - 1);
        servidor.setListNavegacaoVisualizada(null);
        List<Navegacao> result = servidor.getListNavegacaoFull().subList(nav.getId(), servidor.getListNavegacaoFull().size());
        if (result.size() > 12) {
            servidor.setListNavegacaoVisualizada(result.subList(0, 12));
            disableButtonBack = false;
            disableButtonNext = false;
        } else {
            servidor.setListNavegacaoVisualizada(result);
            disableButtonBack = false;
            disableButtonNext = true;
        }
    }

    public void openFiles() {
        String name = barraConfig + navegacao.getArquivo().getName();
        servidor.setDiretorio(servidor.getDiretorio() + name);
        Historico historico = new Historico();
        if (servidor.getListHistorico().isEmpty()) {
            historico.setId(1);
            historico.setNmHistorico(name);
        } else {
            Integer id = servidor.getListHistorico().get(servidor.getListHistorico().size() - 1).getId();
            historico.setId(id + 1);
            historico.setNmHistorico(name);
        }
        servidor.getListHistorico().add(historico);
        validarDiretorioServidor();
    }

    public void adicionarPasta() {
        pastas = new Pastas();
        renderPastas = false;
    }

    public void verificarItensSelecionados() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        boolean existCheck = false;
        for (Navegacao item : servidor.getListNavegacaoVisualizada()) {
            if (item.isArquivoSelecionado()) {
                existCheck = true;
                break;
            }
        }
        if (!existCheck) {
            addErrorMessages("Nenhum Item Selecionado !!");
        } else {
            context.addCallbackParam("sucesso", true);
        }
    }

    public void deletarPasta() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        for (Navegacao item : servidor.getListNavegacaoVisualizada()) {
            if (item.isArquivoSelecionado()) {
                Utils.deleteFiles(item.getArquivo());
            }
        }
        context.addCallbackParam("sucesso", true);
        addSuccessMessage("Item(ns) Deletado com Sucesso !! ");
        validarDiretorioServidor();
    }

    public void confirmarPasta() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);

        if (servidor.getDiretorio() != null && !servidor.getDiretorio().equals("")) {
            File novaPasta = new File(servidor.getDiretorio() + barraConfig + pastas.getNmPasta());
            if (!novaPasta.isDirectory()) {
                novaPasta.mkdirs();
            }
            renderPastas = true;
            context.addCallbackParam("sucesso", true);
            validarDiretorioServidor();
        }
    }

    public void cancelarPasta() {
        renderPastas = true;
        pastas = null;
        validarDiretorioServidor();
    }

    public void confirmarALertUpload() {
        respostaUpload = true;
    }

    public void cancelarALertUpload() {
        respostaUpload = false;
    }

    public void uploadFile(FileUploadEvent event) {

        try {

            RequestContext context = RequestContext.getCurrentInstance();
            context.addCallbackParam("sucesso", false);
            //******************************************************************
            UploadTemp uploadTemp = new UploadTemp();
            uploadTemp.setNmArquivo(event.getFile().getFileName());
            uploadTemp.setByteArquivo(event.getFile().getContents());
            uploadTemp.setCaminhoDestino(servidor.getDiretorio() + barraConfig + uploadTemp.getNmArquivo());

            //************************* Create File Name ***********************
            uploadTemp.setFileUpload(new File(uploadTemp.getCaminhoDestino()));
            //************************* Create File ****************************
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(uploadTemp.getFileUpload()));
            bos.write(uploadTemp.getByteArquivo());
            bos.close();
            if (respostaUpload) {
                //******************************************************************
                File dir = new File(servidor.getDiretorio());

                Utils.unzip(uploadTemp.getFileUpload(), dir);
                //******************************************************************

                File[] arq = dir.listFiles();
                for (File file : arq) {
                    switch (FilenameUtils.getExtension(file.getPath())) {
                        case "jar": {
                            Utils.unzip(file, dir);
                            file.delete();
                            break;
                        }
                        case "war": {
                            Utils.unzip(file, dir);
                            file.delete();
                            break;
                        }
                    }
                }

                uploadTemp.getFileUpload().delete();
                //******************************************************************
            }
            context.addCallbackParam("sucesso", true);
            addSuccessMessage("Upload Gerado com Sucesso!!");

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        validarDiretorioServidor();
    }

    public void downloadFiles() {
        try {

            List<File> listFiles = new ArrayList<>();
            for (Navegacao item : servidor.getListNavegacaoVisualizada()) {
                if (item.isArquivoSelecionado()) {
                    listFiles.add(item.getArquivo());
                }
            }

            //*************************** Create.zip ***************************
            String caminhoZip = Utils.createZip(listFiles, servidor.getDiretorio(), "Arquivos.zip", barraConfig);

            //*************************** Download.zip *************************
            Utils.downloadFile("Arquivos.zip", caminhoZip + "Arquivos.zip", ".zip", FacesContext.getCurrentInstance());

            //*************************** Delete Create.zip ********************
            Utils.deleteFiles(new File(caminhoZip));

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    //******************************* MENU TAREFAS *****************************
    public void carregarTarefas() {
        listTarefas = controleAgendamentoBean.findAllTask();
    }

    public void tarefa() {
        tabProcesso = 2;
        renderTarefas = true;
        carregarTarefas();
    }

    public void adicionarTarefas() {
        controleAgendamento = new ControleAgendamento();
        controleAgendamento.setDataAgendamento(new Date());
        controleAgendamento.setStatusAgendamento("1");
        renderTarefas = false;
    }

    public void editarTarefa() {
        renderTarefas = false;
    }

    public void deletarTarefa() {
        controleAgendamentoBean.delete(controleAgendamento);
        addSuccessMessage("Tarefa removida com Sucesso !!");
        carregarTarefas();
    }

    public void confimarTarefa() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);

        if (controleAgendamento.getDataAgendamento() == null) {
            addErrorMessages("Data Agendamento não Informada!!");
            return;
        }
        if (controleAgendamento.getStatusAgendamento() != null && controleAgendamento.getStatusAgendamento().equals("3") && controleAgendamento.getDataFinalizacao() == null) {
            addErrorMessages("Data Finalização não Informada!!");
            return;
        }
        if (controleAgendamento.getStatusAgendamento() != null && controleAgendamento.getStatusAgendamento().equals("3") && controleAgendamento.getDataFinalizacao() != null && controleAgendamento.getDataFinalizacao().before(controleAgendamento.getDataAgendamento())) {
            controleAgendamento.setDataFinalizacao(null);
            addErrorMessages("Data Finalização não pode ser Menor que a Data de Agendamento !!");
            return;
        }
        if (controleAgendamento.getCliente() == null) {
            addErrorMessages("Cliente não Informado!!");
            return;
        }
        if (controleAgendamento.getStatusAgendamento() != null && !controleAgendamento.getStatusAgendamento().equals("1") && controleAgendamento.getSuporte() == null) {
            addErrorMessages("Registro com Status: " + controleAgendamento.getStatusAgendamentoDesc() + ", Informe um Responsável !!");
            return;
        }
        if (controleAgendamento.getModulos() == null || controleAgendamento.getModulos().getNmModulo().equals("")) {
            addErrorMessages("Modulo não Informado !!");
            return;
        }
        if (controleAgendamento.getGrauSituacao() == null || controleAgendamento.getGrauSituacao().equals("")) {
            addErrorMessages("Grau Situação não Informado !!");
            return;
        }
        if (controleAgendamento.getStatusAgendamento() == null || controleAgendamento.getStatusAgendamento().equals("")) {
            addErrorMessages("Status Agendamento não Informado !!");
            return;
        }
        if (controleAgendamento.getDescricaoTarefa() == null || controleAgendamento.getDescricaoTarefa().equals("")) {
            controleAgendamento.setDataFinalizacao(null);
            addErrorMessages("Descrição da Tarefa não Informada !!");
            return;
        }

        if (controleAgendamento.getId() == null) {
            controleAgendamentoBean.create(controleAgendamento);
            addSuccessMessage("Tarefa Inserida com Sucesso !!");
        } else {
            controleAgendamentoBean.update(controleAgendamento);
            addSuccessMessage("Tarefa Atualizada com Sucesso !!");
        }
        context.addCallbackParam("sucesso", true);
        cancelarTarefa();
    }

    public void cancelarTarefa() {
        controleAgendamento = null;
        renderTarefas = true;
        carregarTarefas();
    }

    //******************************* MENU ERROS *******************************
    public void carregarErrosSistema() {
        listErrosSistema = controleAgendamentoBean.findAllError();
    }

    public void errosSistemasIniciar() {
        tabProcesso = 3;
        renderErrosSistema = true;
        errosSistema = null;
        carregarErrosSistema();
    }

    public void adicionarErrosSistema() {
        renderErrosSistema = false;
        errosSistema = new ErrosSistema();
        errosSistema.setDataRegistro(new Date());
        errosSistema.setStatus("1");
    }

    public void editarErrosSistema() {
        renderErrosSistema = false;
    }

    public void deletarErrosSistema() {
        controleAgendamentoBean.delete(errosSistema);
        addSuccessMessage("Registro Removido com Sucesso !!");
        carregarErrosSistema();
    }

    public void confirmarErrosSistema() {
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("sucesso", false);
        if (errosSistema.getDataRegistro() == null) {
            addErrorMessages("Data Registro não Informada!!");
            return;
        }
        if (errosSistema.getStatus() != null && errosSistema.getStatus().equals("3") && errosSistema.getDataFinalizacao() == null) {
            addErrorMessages("Data Finalização não Informada!!");
            return;
        }
        if (errosSistema.getStatus() != null && errosSistema.getStatus().equals("3") && errosSistema.getDataFinalizacao() != null && errosSistema.getDataFinalizacao().before(errosSistema.getDataRegistro())) {
            errosSistema.setDataFinalizacao(null);
            addErrorMessages("Data Finalização nao pode ser menor que a Data de Registro !!");
            return;
        }

        if (errosSistema.getStatus() != null && !errosSistema.getStatus().equals("1") && errosSistema.getSuporte() == null) {
            addErrorMessages("Registro com Status: " + errosSistema.getStatusDesc() + ", Informe um Responsável !!");
            return;
        }
        if (errosSistema.getModulos() == null || errosSistema.getModulos().getNmModulo().equals("")) {
            addErrorMessages("Modulo não Informado !!");
            return;
        }
        if (errosSistema.getPrioridade() == null || errosSistema.getPrioridade().equals("")) {
            addErrorMessages("Prioridade não Informado !!");
            return;
        }
        if (errosSistema.getStatus() == null || errosSistema.getStatus().equals("")) {
            addErrorMessages("Status Agendamento não Informado !!");
            return;
        }
        if (errosSistema.getDescricao() == null || errosSistema.getDescricao().equals("")) {
            addErrorMessages("Descrição não Informada !!");
            return;
        }

        if (errosSistema.getId() == null) {
            controleAgendamentoBean.create(errosSistema);
            addSuccessMessage("Registro Inserido com Sucesso !!");
        } else {
            controleAgendamentoBean.update(errosSistema);
            addSuccessMessage("Registro Atualizado com Sucesso !!");
        }
        context.addCallbackParam("sucesso", true);
        cancelarErrosSistema();
    }

    public void cancelarErrosSistema() {
        errosSistema = null;
        renderErrosSistema = true;
        carregarErrosSistema();
    }

    public void statusOnChange() {
        if (errosSistema.getStatus().equals("1") || errosSistema.getStatus().equals("")) {
            errosSistema.setSuporte(null);
        }
    }

    //******************************* MENU LOGS ********************************
    public void carregarLogsSistema() {
        listaLog = controleAgendamentoBean.findAllLogs();
    }

    public void logsSistemaIniciar() {
        tabProcesso = 4;
        renderLogsSistema = true;
        carregarLogsSistema();

    }

    //***************************** mensagem ***********************************
    public void addErrorMessages(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addSuccessMessage(String mensagem) {
        FacesMessage msg;
        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, mensagem, "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    //***************************** get && setts *******************************
    public ControleAgendamentoBean getControleAgendamentoBean() {
        return controleAgendamentoBean;
    }

    public void setControleAgendamentoBean(ControleAgendamentoBean controleAgendamentoBean) {
        this.controleAgendamentoBean = controleAgendamentoBean;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Suporte getSuporte() {
        return suporte;
    }

    public void setSuporte(Suporte suporte) {
        this.suporte = suporte;
    }

    public ControleAgendamento getControleAgendamento() {
        return controleAgendamento;
    }

    public void setControleAgendamento(ControleAgendamento controleAgendamento) {
        this.controleAgendamento = controleAgendamento;
    }

    public List<Cliente> getListCliente() {
        if (listCliente == null) {
            listCliente = new ArrayList<>();
        }
        return listCliente;
    }

    public void setListCliente(List<Cliente> listCliente) {
        this.listCliente = listCliente;
    }

    public List<Suporte> getListSuporte() {
        if (listSuporte == null) {
            listSuporte = new ArrayList<>();
        }
        return listSuporte;
    }

    public void setListSuporte(List<Suporte> listSuporte) {
        this.listSuporte = listSuporte;
    }

    public List<ControleAgendamento> getListTarefas() {
        if (listTarefas == null) {
            listTarefas = new ArrayList<>();
        }
        return listTarefas;
    }

    public void setListTarefas(List<ControleAgendamento> listTarefas) {
        this.listTarefas = listTarefas;
    }

    public boolean isRenderCliente() {
        return renderCliente;
    }

    public void setRenderCliente(boolean renderCliente) {
        this.renderCliente = renderCliente;
    }

    public boolean isRenderSuporte() {
        return renderSuporte;
    }

    public void setRenderSuporte(boolean renderSuporte) {
        this.renderSuporte = renderSuporte;
    }

    public boolean isRenderTarefas() {
        return renderTarefas;
    }

    public void setRenderTarefas(boolean renderTarefas) {
        this.renderTarefas = renderTarefas;
    }

    public boolean isDisableIpSuporte() {
        return disableIpSuporte;
    }

    public void setDisableIpSuporte(boolean disableIpSuporte) {
        this.disableIpSuporte = disableIpSuporte;
    }

    public boolean isDisableinputEditarSuporte() {
        return disableinputEditarSuporte;
    }

    public void setDisableinputEditarSuporte(boolean disableinputEditarSuporte) {
        this.disableinputEditarSuporte = disableinputEditarSuporte;
    }

    public Modulos getModulos() {
        return modulos;
    }

    public void setModulos(Modulos modulos) {
        this.modulos = modulos;
    }

    public boolean isRenderModulos() {
        return renderModulos;
    }

    public void setRenderModulos(boolean renderModulos) {
        this.renderModulos = renderModulos;
    }

    public List<Modulos> getListModulos() {
        return listModulos;
    }

    public void setListModulos(List<Modulos> listModulos) {
        this.listModulos = listModulos;
    }

    public Integer getTabCadastro() {
        return tabCadastro;
    }

    public void setTabCadastro(Integer tabCadastro) {
        this.tabCadastro = tabCadastro;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public boolean isRenderTelefoneCliente() {
        return renderTelefoneCliente;
    }

    public void setRenderTelefoneCliente(boolean renderTelefoneCliente) {
        this.renderTelefoneCliente = renderTelefoneCliente;
    }

    public List<Telefone> getListTelefonesRemovido() {
        if (listTelefonesRemovido == null) {
            listTelefonesRemovido = new ArrayList<>();
        }
        return listTelefonesRemovido;
    }

    public void setListTelefonesRemovido(List<Telefone> listTelefonesRemovido) {
        this.listTelefonesRemovido = listTelefonesRemovido;
    }

    public String getDataAtual() {
        return dataAtual;
    }

    public void setDataAtual(String dataAtual) {
        this.dataAtual = dataAtual;
    }

    public ErrosSistema getErrosSistema() {
        return errosSistema;
    }

    public void setErrosSistema(ErrosSistema errosSistema) {
        this.errosSistema = errosSistema;
    }

    public Integer getTabProcesso() {
        return tabProcesso;
    }

    public void setTabProcesso(Integer tabProcesso) {
        this.tabProcesso = tabProcesso;
    }

    public boolean isRenderErrosSistema() {
        return renderErrosSistema;
    }

    public void setRenderErrosSistema(boolean renderErrosSistema) {
        this.renderErrosSistema = renderErrosSistema;
    }

    public List<ErrosSistema> getListErrosSistema() {
        return listErrosSistema;
    }

    public void setListErrosSistema(List<ErrosSistema> listErrosSistema) {
        this.listErrosSistema = listErrosSistema;
    }

    public Servidor getServidor() {
        return servidor;
    }

    public void setServidor(Servidor servidor) {
        this.servidor = servidor;
    }

    public Pastas getPastas() {
        return pastas;
    }

    public void setPastas(Pastas pastas) {
        this.pastas = pastas;
    }

    public boolean isRenderServidor() {
        return renderServidor;
    }

    public void setRenderServidor(boolean renderServidor) {
        this.renderServidor = renderServidor;
    }

    public boolean isRenderPastas() {
        return renderPastas;
    }

    public void setRenderPastas(boolean renderPastas) {
        this.renderPastas = renderPastas;
    }

    public List<Pastas> getListPastas() {
        return listPastas;
    }

    public void setListPastas(List<Pastas> listPastas) {
        this.listPastas = listPastas;
    }

    public Integer getStatusValidacao() {
        return statusValidacao;
    }

    public void setStatusValidacao(Integer statusValidacao) {
        this.statusValidacao = statusValidacao;
    }

    public boolean isDisableButtonBack() {
        return disableButtonBack;
    }

    public void setDisableButtonBack(boolean disableButtonBack) {
        this.disableButtonBack = disableButtonBack;
    }

    public boolean isDisableButtonNext() {
        return disableButtonNext;
    }

    public void setDisableButtonNext(boolean disableButtonNext) {
        this.disableButtonNext = disableButtonNext;
    }

    public Navegacao getNavegacao() {
        return navegacao;
    }

    public void setNavegacao(Navegacao navegacao) {
        this.navegacao = navegacao;
    }

    public String getBarraConfig() {
        return barraConfig;
    }

    public void setBarraConfig(String barraConfig) {
        this.barraConfig = barraConfig;
    }

    public boolean isDisableInputServer() {
        return disableInputServer;
    }

    public void setDisableInputServer(boolean disableInputServer) {
        this.disableInputServer = disableInputServer;
    }

    /**
     * @return the renderLogsSistema
     */
    public boolean isRenderLogsSistema() {
        return renderLogsSistema;
    }

    /**
     * @param renderLogsSistema the renderLogsSistema to set
     */
    public void setRenderLogsSistema(boolean renderLogsSistema) {
        this.renderLogsSistema = renderLogsSistema;
    }

    /**
     * @return the listaLog
     */
    public List<Log> getListaLog() {
        if (listaLog == null) {
            listaLog = new ArrayList<>();
        }
        return listaLog;
    }

    /**
     * @param listaLog the listaLog to set
     */
    public void setListaLog(List<Log> listaLog) {
        this.listaLog = listaLog;
    }

    public boolean isRespostaUpload() {
        return respostaUpload;
    }

    public void setRespostaUpload(boolean respostaUpload) {
        this.respostaUpload = respostaUpload;
    }

}
