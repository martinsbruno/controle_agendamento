package br.com.softline.padrao.converter;

import br.com.softline.padrao.model.Suporte;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author bruno martins
 */
@FacesConverter(forClass = Suporte.class)
public class SuporteConverter extends ConverterUtil implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        Integer id = Integer.parseInt(string);
        Suporte obj = (Suporte) getBean().findByPrimaryKey(Suporte.class, id);
        return obj;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Suporte obj = (Suporte) o;
        return obj.getId().toString();
    }
}
