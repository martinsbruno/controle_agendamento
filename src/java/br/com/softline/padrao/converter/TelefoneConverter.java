package br.com.softline.padrao.converter;

import br.com.softline.padrao.model.Telefone;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author bruno martins
 */
@FacesConverter(forClass = Telefone.class)
public class TelefoneConverter extends ConverterUtil implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        Integer id = Integer.parseInt(string);
        Telefone obj = (Telefone) getBean().findByPrimaryKey(Telefone.class, id);
        return obj;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Telefone obj = (Telefone) o;
        return obj.getId().toString();
    }
}
