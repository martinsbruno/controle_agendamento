/*
 * Decimal.java
 *
 * Created on 19 de Novembro de 2007, 17:09
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package br.com.softline.padrao.converter;

import java.text.NumberFormat;
import java.util.Locale;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Adam
 */
@FacesConverter(value = "decimal")
public class Decimal implements Converter {

    public Decimal() {
    }

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String valorTela) throws ConverterException {
        Double valorRetorno;
        try {

            if (valorTela == null || valorTela.equals("")) {
                return null;
            } else {
                valorTela = valorTela.replace(".", "");
                valorTela = valorTela.replace(",", ".");
                valorRetorno = Double.valueOf(valorTela);
                return valorRetorno;
            }

        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object valorTela) throws ConverterException {

        if (valorTela == null || valorTela.equals("")) {
            return null;

        } else {
            NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(2);

            return nf.format(Double.valueOf(valorTela.toString()));
        }

    }
}
