package br.com.softline.padrao.converter;

import br.com.softline.padrao.model.Modulos;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author bruno martins
 */
@FacesConverter(forClass = Modulos.class)
public class ModuloConverter extends ConverterUtil implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        Integer id = Integer.parseInt(string);
        Modulos obj = (Modulos) getBean().findByPrimaryKey(Modulos.class, id);
        return obj;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Modulos obj = (Modulos) o;
        return obj.getId().toString();
    }
}
