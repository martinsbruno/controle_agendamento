package br.com.softline.padrao.converter;

import br.com.softline.padrao.model.Cliente;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author bruno martins
 */
@FacesConverter(forClass = Cliente.class)
public class ClienteConverter extends ConverterUtil implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string == null || string.equals("")) {
            return null;
        }
        Integer id = Integer.parseInt(string);
        Cliente obj = (Cliente) getBean().findByPrimaryKey(Cliente.class, id);
        return obj;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        Cliente obj = (Cliente) o;
        return obj.getId().toString();
    }
}
