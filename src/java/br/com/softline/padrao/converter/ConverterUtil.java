/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.converter;

import br.com.softline.padrao.bean.ControleAgendamentoBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author adam
 */
public class ConverterUtil {

    public ConverterUtil() {
    }

    protected ControleAgendamentoBean getBean() {
        try {
            Context c = new InitialContext();
            return (ControleAgendamentoBean) c.lookup("java:global/Controle_Agendamento/ControleAgendamentoBean!br.com.softline.padrao.bean.ControleAgendamentoBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
