/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.model;

import br.com.sotline.padrao.temp.Historico;
import br.com.sotline.padrao.temp.Navegacao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author bruno-martins
 */
@Entity
@Table(name = "cad_servidor")
public class Servidor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String diretorio;
    @Transient
    private List<Navegacao> listNavegacaoFull;
    @Transient
    private List<Navegacao> listNavegacaoVisualizada;
    @Transient
    private List<Historico> listHistorico;

    //******************************** Equals && Hashcode **********************
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servidor other = (Servidor) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    //******************************* get && setts *****************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiretorio() {
        return diretorio;
    }

    public void setDiretorio(String diretorio) {
        this.diretorio = diretorio;
    }

    public List<Navegacao> getListNavegacaoFull() {
        if (listNavegacaoFull == null) {
            listNavegacaoFull = new ArrayList<>();
        }
        return listNavegacaoFull;
    }

    public void setListNavegacaoFull(List<Navegacao> listNavegacaoFull) {
        this.listNavegacaoFull = listNavegacaoFull;
    }

    public List<Navegacao> getListNavegacaoVisualizada() {
        if (listNavegacaoVisualizada == null) {
            listNavegacaoVisualizada = new ArrayList<>();
        }
        return listNavegacaoVisualizada;
    }

    public void setListNavegacaoVisualizada(List<Navegacao> listNavegacaoVisualizada) {
        this.listNavegacaoVisualizada = listNavegacaoVisualizada;
    }

    public List<Historico> getListHistorico() {
        if (listHistorico == null) {
            listHistorico = new ArrayList<>();
        }
        return listHistorico;
    }

    public void setListHistorico(List<Historico> listHistorico) {
        this.listHistorico = listHistorico;
    }

}
