/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author bruno-martins
 */
@Entity
@Table(name = "cad_cliente")
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nmCliente;
    private String nmEmpresaCliente;
//    private String cpfCnpjCliente;
    @Transient
    private String telefoneCliente;

    @OrderBy("id")
    @OneToMany(cascade = CascadeType.ALL)
    private List<Telefone> listTelefoneCliente;

    //******************************* equals && hashcode ***********************
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    //******************************** get && setts ****************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNmCliente() {
        return nmCliente;
    }

    public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
    }

    public String getNmEmpresaCliente() {
        return nmEmpresaCliente;
    }

    public void setNmEmpresaCliente(String nmEmpresaCliente) {
        this.nmEmpresaCliente = nmEmpresaCliente;
    }

    public String getTelefoneCliente() {
        return telefoneCliente;
    }

    public void setTelefoneCliente(String telefoneCliente) {
        this.telefoneCliente = telefoneCliente;
    }

    public List<Telefone> getListTelefoneCliente() {
        if (listTelefoneCliente == null) {
            listTelefoneCliente = new ArrayList<>();
        }
        return listTelefoneCliente;
    }

    public void setListTelefoneCliente(List<Telefone> listTelefoneCliente) {
        this.listTelefoneCliente = listTelefoneCliente;
    }

//    public String getCpfCnpjCliente() {
//        return cpfCnpjCliente;
//    }
//
//    public void setCpfCnpjCliente(String cpfCnpjCliente) {
//        this.cpfCnpjCliente = cpfCnpjCliente;
//    }

}
