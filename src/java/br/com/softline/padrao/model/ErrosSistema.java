/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bruno-martins
 */
@Entity
@Table(name = "cad_errossistema")
public class ErrosSistema implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Temporal(TemporalType.DATE)
    private Date dataRegistro;
    @Temporal(TemporalType.DATE)
    private Date dataFinalizacao;
    @ManyToOne
    private Cliente cliente;
    @ManyToOne
    private Modulos modulos;
    @ManyToOne
    private Suporte suporte;
    private String descricao;
    /**
     * 1-baixo; 2-relativo; 3-critico;
     */
    private String prioridade;
    /**
     * 1-pendente;2-em andamento;3-concluido
     */
    private String status;

    public String getPrioridadeDesc() {
        String result = "";
        switch (getPrioridade()) {
            case "1": {
                result = "Baixo";
                break;
            }
            case "2": {
                result = "Relativo";
                break;
            }
            case "3": {
                result = "Crítico";
                break;
            }
        }
        return result;
    }

    public String getStatusDesc() {
        String result = "";
        switch (getStatus()) {
            case "1": {
                result = "Pendente";
                break;
            }
            case "2": {
                result = "Em Andamento";
                break;
            }
            case "3": {
                result = "Concluido";
                break;
            }
        }
        return result;
    }

    //******************************* equals && hashcode ***********************
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ErrosSistema other = (ErrosSistema) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    //******************************* get && setts *****************************

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the dataRegistro
     */
    public Date getDataRegistro() {
        return dataRegistro;
    }

    /**
     * @param dataRegistro the dataRegistro to set
     */
    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    /**
     * @return the dataFinalizacao
     */
    public Date getDataFinalizacao() {
        return dataFinalizacao;
    }

    /**
     * @param dataFinalizacao the dataFinalizacao to set
     */
    public void setDataFinalizacao(Date dataFinalizacao) {
        this.dataFinalizacao = dataFinalizacao;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the modulos
     */
    public Modulos getModulos() {
        return modulos;
    }

    /**
     * @param modulos the modulos to set
     */
    public void setModulos(Modulos modulos) {
        this.modulos = modulos;
    }

    /**
     * @return the suporte
     */
    public Suporte getSuporte() {
        return suporte;
    }

    /**
     * @param suporte the suporte to set
     */
    public void setSuporte(Suporte suporte) {
        this.suporte = suporte;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * 1-baixo; 2-relativo; 3-critico;
     * @return the prioridade
     */
    public String getPrioridade() {
        return prioridade;
    }

    /**
     * 1-baixo; 2-relativo; 3-critico;
     * @param prioridade the prioridade to set
     */
    public void setPrioridade(String prioridade) {
        this.prioridade = prioridade;
    }

    /**
     * 1-pendente;2-em andamento;3-concluido
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 1-pendente;2-em andamento;3-concluido
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
    

}
