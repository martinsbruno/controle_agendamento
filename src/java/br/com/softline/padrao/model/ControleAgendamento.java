/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.softline.padrao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bruno-martins
 */
@Entity
@Table(name = "cad_controleagendamento")
public class ControleAgendamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String descricaoTarefa;
    @ManyToOne
    private Suporte suporte;
    @ManyToOne
    private Cliente cliente;
    @ManyToOne
    private Modulos modulos;
    /**
     * 1-baixo; 2-relativo; 3-critico;
     */
    private String grauSituacao;
    /**
     * 1-pendente;2-em andamento;3-concluido
     */
    private String statusAgendamento;
    @Temporal(TemporalType.DATE)
    private Date dataAgendamento;
    @Temporal(TemporalType.TIME)
    private Date horaAgendamento;
    @Temporal(TemporalType.DATE)
    private Date dataFinalizacao;
    @Temporal(TemporalType.TIME)
    private Date horaFinalizacao;

    public String getGrauSituacaoDesc() {
        String result = "";
        switch (grauSituacao) {
            case "1": {
                result = "Baixo";
                break;
            }
            case "2": {
                result = "Relativo";
                break;
            }
            case "3": {
                result = "Crítico";
                break;
            }
        }
        return result;
    }

    public String getStatusAgendamentoDesc() {
        String result = "";
        switch (statusAgendamento) {
            case "1": {
                result = "Pendente";
                break;
            }
            case "2": {
                result = "Em Andamento";
                break;
            }
            case "3": {
                result = "Concluido";
                break;
            }
        }
        return result;
    }

    //********************************* equals && hashcode *********************
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ControleAgendamento other = (ControleAgendamento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    //********************************* get && setts ***************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the descricaoTarefa
     */
    public String getDescricaoTarefa() {
        return descricaoTarefa;
    }

    /**
     * @param descricaoTarefa the descricaoTarefa to set
     */
    public void setDescricaoTarefa(String descricaoTarefa) {
        this.descricaoTarefa = descricaoTarefa;
    }

    /**
     * @return the suporte
     */
    public Suporte getSuporte() {
        return suporte;
    }

    /**
     * @param suporte the suporte to set
     */
    public void setSuporte(Suporte suporte) {
        this.suporte = suporte;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * 1-baixo; 2-relativo; 3-critico;
     *
     * @return the grauSituacao
     */
    public String getGrauSituacao() {
        return grauSituacao;
    }

    /**
     * 1-baixo; 2-relativo; 3-critico;
     *
     * @param grauSituacao the grauSituacao to set
     */
    public void setGrauSituacao(String grauSituacao) {
        this.grauSituacao = grauSituacao;
    }

    /**
     * 1-pendente;2-em andamento;3-concluido
     *
     * @return the statusAgendamento
     */
    public String getStatusAgendamento() {
        return statusAgendamento;
    }

    /**
     * 1-pendente;2-em andamento;3-concluido
     *
     * @param statusAgendamento the statusAgendamento to set
     */
    public void setStatusAgendamento(String statusAgendamento) {
        this.statusAgendamento = statusAgendamento;
    }

    /**
     * @return the dataAgendamento
     */
    public Date getDataAgendamento() {
        return dataAgendamento;
    }

    /**
     * @param dataAgendamento the dataAgendamento to set
     */
    public void setDataAgendamento(Date dataAgendamento) {
        this.dataAgendamento = dataAgendamento;
    }

    /**
     * @return the horaAgendamento
     */
    public Date getHoraAgendamento() {
        return horaAgendamento;
    }

    /**
     * @param horaAgendamento the horaAgendamento to set
     */
    public void setHoraAgendamento(Date horaAgendamento) {
        this.horaAgendamento = horaAgendamento;
    }

    /**
     * @return the dataFinalizacao
     */
    public Date getDataFinalizacao() {
        return dataFinalizacao;
    }

    /**
     * @param dataFinalizacao the dataFinalizacao to set
     */
    public void setDataFinalizacao(Date dataFinalizacao) {
        this.dataFinalizacao = dataFinalizacao;
    }

    /**
     * @return the horaFinalizacao
     */
    public Date getHoraFinalizacao() {
        return horaFinalizacao;
    }

    /**
     * @param horaFinalizacao the horaFinalizacao to set
     */
    public void setHoraFinalizacao(Date horaFinalizacao) {
        this.horaFinalizacao = horaFinalizacao;
    }

    public Modulos getModulos() {
        return modulos;
    }

    public void setModulos(Modulos modulos) {
        this.modulos = modulos;
    }

}
