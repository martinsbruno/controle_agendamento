package br.com.softline.servico;

import br.com.softline.padrao.bean.ControleAgendamentoBean;
import br.com.softline.padrao.model.Log;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author alexisbrabo
 */
@WebService
public class ServicoLogs {

    @EJB
    ControleAgendamentoBean bean;

    @WebMethod
    public void getXml(byte[] bytes) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream("/home/servidoruser/softline/logsRecebidos/teste.xml");
        fos.write(bytes);
        fos.close();

        XStream xStream = new XStream(new DomDriver());
        xStream.alias("log", Log.class);
        Log l = (Log) xStream.fromXML(new FileInputStream(new File("/home/servidoruser/softline/logsRecebidos/teste.xml")));

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        String data = dateFormat.format(l.getDataHoraLog());
        String novoCaminho = "/home/servidoruser/softline/logsRecebidos/" + l.getIdServidor() + data + ".xml";
        new File("/home/servidoruser/softline/logsRecebidos/teste.xml").renameTo(new File("/home/servidoruser/softline/logsRecebidos/" + l.getIdServidor() + data + ".xml"));
        l.setCaminhoArquivo(novoCaminho);
        bean.create(l);
    }
}
