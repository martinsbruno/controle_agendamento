/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sotline.padrao.temp;

/**
 *
 * @author bruno-martins
 */
public class Historico {
    private Integer id;
    private String nmHistorico;

    //**************************** get && setts ********************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNmHistorico() {
        return nmHistorico;
    }

    public void setNmHistorico(String nmHistorico) {
        this.nmHistorico = nmHistorico;
    }
}
