/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sotline.padrao.temp;

import java.io.File;
import java.util.StringTokenizer;

/**
 *
 * @author bruno-martins
 */
public class UploadTemp {

    private String id;
    private String nmArquivo;
    private String caminhoOrigem;
    private String caminhoDestino;
    private byte[] byteArquivo;
    private File fileUpload;

    public String getNmArquivo() {

        return nmArquivo;
    }

    public void setNmArquivo(String nmArquivo) {
        this.nmArquivo = nmArquivo;
    }

    public String getCaminhoOrigem() {
        return caminhoOrigem;
    }

    public void setCaminhoOrigem(String caminhoOrigem) {
        this.caminhoOrigem = caminhoOrigem;
    }

    public byte[] getByteArquivo() {
        return byteArquivo;
    }

    public void setByteArquivo(byte[] byteArquivo) {
        this.byteArquivo = byteArquivo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getCaminhoDestino() {
        return caminhoDestino;
    }

    public void setCaminhoDestino(String caminhoDestino) {
        this.caminhoDestino = caminhoDestino;
    }

}
