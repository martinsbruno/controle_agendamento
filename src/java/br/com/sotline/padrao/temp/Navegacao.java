/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sotline.padrao.temp;

import java.io.File;

/**
 *
 * @author bruno-martins
 */
public class Navegacao {

    private Integer id;
    private boolean arquivoSelecionado = false;
    private File arquivo;

    //****************************** get && setts ******************************
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public File getArquivo() {
        return arquivo;
    }

    public void setArquivo(File arquivo) {
        this.arquivo = arquivo;
    }

    public boolean isArquivoSelecionado() {
        return arquivoSelecionado;
    }

    public void setArquivoSelecionado(boolean arquivoSelecionado) {
        this.arquivoSelecionado = arquivoSelecionado;
    }

}
